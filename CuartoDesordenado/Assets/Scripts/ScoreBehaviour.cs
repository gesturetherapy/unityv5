﻿using UnityEngine;
using System.Collections;

//! ScoreBehaviour class
/*! This class represents the behaviour of the "score" GameObject after an "object" GameObject
 * is set in the goal position. The score object moves directly upwards the creation of the object.
 */
public class ScoreBehaviour : MonoBehaviour {

	Vector3 initialPosition; /*!< The initial position of the instantiation of the GameObject */
	Vector3 goalPosition; /*!< Final position of the "score" GameObject */

	//! Start() function
	/*! This function is called on every Scene change in Unity 
	 * it initializes the value of the variables described above
	 * excepting the public variables initialized in the Unity editor
	 */
	void Start () {
		initialPosition = this.transform.position;
		goalPosition = initialPosition + new Vector3(0,1,0);
	}
	
	//! Update() function
	/*! This function is called every frame it refreshes the
	 * movement, lifetime and the destruction of GameObjects "Score"
	 */
	void Update () {
		this.transform.position = Vector3.MoveTowards(this.transform.position,goalPosition,0.02f);
		if(this.transform.position == goalPosition){
			Destroy (this.gameObject);
		}
	}
}
