﻿using UnityEngine;
using System.Collections;
//! ParticleBehaviour class
/*! This class represents the particles shown while the "kid" GameObject is making a mess
 * or while every "object" in the room is in their goal position
 */
public class ParticleBehaviour : MonoBehaviour {

	float lifeTime; /*!< This variable is the maximum life time of the particles */
	float spanTime; /*!< This variable is the current life time of the particles */

	//! Start() function
	/*! This function is called on every Scene change in Unity 
	 * it initializes the value of the variables described above
	 * excepting the public variables initialized in the Unity editor
	 */
	void Start () {
		spanTime = 0.0f;
		lifeTime = 0.25f;
	}
	
	//! Update() function
	/*! This function is called every frame it refreshes the
	 * movement, lifetime and the destruction of GameObjects "Score"
	 */
	void Update () {
		spanTime += Time.deltaTime;
		if(spanTime >= lifeTime){
			Destroy(this.gameObject);
		}
	}
}
