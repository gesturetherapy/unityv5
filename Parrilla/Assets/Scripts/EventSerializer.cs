﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;

public class EventSerializer : MonoBehaviour {

	JSON serialEvent = new JSON();
	List<JSON> objectsList = new List<JSON>();
	List<JSON> serialEventsList = new List<JSON>();
	public GameObject serializerObject;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (serialEventsList.ToArray().Length != 0){
			serializerObject.SendMessage("addEventList",serialEventsList,SendMessageOptions.RequireReceiver);
			serialEventsList.Clear();
		}
	}

	void OnCollisionStay2D(Collision2D collision2D){
		objectsList = new List<JSON>();
		serialEvent = new JSON();
		serialEvent["id"] = this.gameObject.GetInstanceID().ToString();
		serialEvent["type"] = "2D collisions";
		JSON tempInfo = new JSON();
		foreach(ContactPoint2D contactPoint2D in collision2D.contacts){
			tempInfo = new JSON();
			tempInfo["id"] = contactPoint2D.otherCollider.gameObject.GetInstanceID().ToString();
			tempInfo["point"] = contactPoint2D.point.ToString();
			objectsList.Add(tempInfo);
		}
		serialEvent["relative velocity"] = collision2D.relativeVelocity.ToString();
		serialEvent["objects"] = objectsList.ToArray();
		serialEventsList.Add(serialEvent);
	}

	void OnCollisionStay(Collision collision){
		objectsList = new List<JSON>();
		serialEvent = new JSON();
		serialEvent["id"] = this.gameObject.GetInstanceID().ToString();
		serialEvent["type"] = "collisions";
		JSON tempInfo = new JSON();
		foreach(ContactPoint contactPoint in collision.contacts){
			tempInfo = new JSON();
			tempInfo["id"] = contactPoint.otherCollider.gameObject.GetInstanceID().ToString();
			tempInfo["point"] = contactPoint.point.ToString();
			objectsList.Add(tempInfo);
		}
		serialEvent["relative velocity"] = collision.relativeVelocity.ToString();
		serialEvent["objects"] = objectsList.ToArray();
		serialEventsList.Add(serialEvent);
	}

	void OnTriggerStay2D(Collider2D collider2D){
		serialEvent = new JSON();
		serialEvent["id"] = this.gameObject.GetInstanceID().ToString();
		serialEvent["type"] = "2D triggers";
		serialEvent["object id"] = collider2D.gameObject.GetInstanceID().ToString();
		serialEventsList.Add(serialEvent);
	}

	void onTriggerStay(Collider collider){
		serialEvent = new JSON();
		serialEvent["id"] = this.gameObject.GetInstanceID().ToString();
		serialEvent["type"] = "triggers";
		serialEvent["object id"] = collider.gameObject.GetInstanceID().ToString();
		serialEventsList.Add(serialEvent);
	}

}
