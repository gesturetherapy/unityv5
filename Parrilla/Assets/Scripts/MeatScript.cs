﻿using UnityEngine;
using System.Collections;

public class MeatScript : MonoBehaviour 
{

	Animator anim;
	float maxTimeSeconds = 4.0f;
	float grillingTime = 0.0f;
	Camera mainCamera;

	// Use this for initialization
	void Start () 
	{
		anim = GetComponent<Animator>();
		mainCamera = (Camera) FindObjectOfType(typeof(Camera));
	}
	
	// Update is called once per frame
	void Update () 
	{
		grillingTime +=  Time.deltaTime;
		anim.SetFloat("Grill Time", grillingTime/maxTimeSeconds);

	}

	void destroyMeat()
	{
		Destroy(this.gameObject);
		mainCamera.GetComponent<CameraScript>().setErrors(mainCamera.GetComponent<CameraScript>().getErrors() + 1);
	}

}

