﻿using UnityEngine;
using System.Collections;

//! AimControllerScript class
/*! This script works as a controller for the avatar "aim" and using the "PDController" for the pressure sensor of the Gripper
 * it estimates the forces using this sensor too. The score using multipliers or combos is calculated and updated here.
 */
public class AimControllerScript : MonoBehaviour {
	
	private static Animator anim; /*!< This variable represents the animation controller of the "aim" GameObject */
	Collider2D[] colliders; /*!< Array of trigger colliders interacting with the "aim" GameObject collider */
	public float minForce; /*!< This variable is the minimum force from the pressure sensor*/
	public float force; /*!< This variable represents the current force of the pressure sensor */
	public float maxForce; /*!< This variable represents the maximum force of the pressure sensor */
	int chargingSpeed; /*!< This variable is a constant for the increasing speed of the force bar on the GUI */
	int dischargingSpeed; /*!< This variable is a constant for the decreasing speed of the force bar on the GUI */
	float score; /*!< This variable is the current score for the GUI */
	float tempScore; /*!< This variable is the calculated score using the combo multipliers */
	private static bool paused; /*!< This flag stops the time counter after each shot */
	private static bool throwing; /*!< This flags is raised while the sensor pressure is being pressed */
	bool interactive; /*!< This flag is raised if the end of turn has finished*/
	public AudioClip audioAim; /*!< This Audio clip is played when this collider interacts with the colliders of the "cube" GameObjects */
	
	//! Start() function
	/*! This function is called on every Scene change in Unity 
	 * it initializes the value of the variables described above
	 * excepting the public variables initialized in the Unity editor
	 */
	void Start () {
		chargingSpeed = 500;
		dischargingSpeed = 3000;
		score = 0.0f;
		tempScore = 0.0f;
		force = minForce;
		Cursor.visible = false;
		anim = this.GetComponent<Animator>();
		paused = false;
		throwing = false;
		interactive = true;
	}
	
	//! Update() function
	/*! This function is called every frame, it refreshes the position of the "aim" GameObject,
	 * pressure sensor and animation while interacting with the "cube" GameObjects
	 */
	void Update () {
		if(!paused){
			if(transform.position.x >= 7.5) transform.position = new Vector3(7.49f,transform.position.y,transform.position.z);
			if(transform.position.x <= -7.5) transform.position = new Vector3(-7.49f,transform.position.y,transform.position.z);
			if(transform.position.y >= 5.5) transform.position = new Vector3(transform.position.x,5.49f,transform.position.z);
			if(transform.position.y <= -5.5) transform.position = new Vector3(transform.position.x,-5.49f,transform.position.z);
			if((transform.position.x < 7.5 && transform.position.x > -7.5) && (transform.position.y < 5.5 && transform.position.y > -5.5))
				transform.position = new Vector3(7.5f*Input.GetAxis("Horizontal"),5.5f*Input.GetAxis("Vertical"));
			if(interactive){
				if(this.GetComponent<PDController>().Hold()){
					throwing = true;
				}else{
					if(throwing){
						Shoot(Physics2D.OverlapCircleAll(this.GetComponent<CircleCollider2D>().transform.position,this.GetComponent<CircleCollider2D>().radius));
					}
					throwing = false;
				}
			}
		}
	}

	//! FixedUpdate() function
	/*! This function is called fixed time step, it refreshes the position the force in the force bar 
	 * and increases it or decreases it depending of the input of the "Gripper"
	 */
	void FixedUpdate(){
		if(interactive){
			if(this.GetComponent<PDController>().Hold()){
				if(force < maxForce){
					force += chargingSpeed * Time.fixedDeltaTime;
					if(force > maxForce)
						force = maxForce;
				}
			} else{
				if(force > minForce){
					force -= dischargingSpeed * Time.fixedDeltaTime;
					if (force < minForce)
						force = minForce;
				}
			}
		}
	}

	//! Public function Pause()
	/*! This function stops/resumes the countdown of the time each time a stone has being thrown
	 */
	public void Pause(bool pause){
		paused = pause;
	}

	//! Public function setForce()
	/*! This function rereshes the value of the force for the GUI and other external scripts
	 */
	public void setForce(float thisForce){
		force = thisForce;
	}

	//! Public function setInteractive()
	/*! This function raises the flag "interactive" when called. The player is able to interact when the game
	 * after all the "cube" GameObjects are not moving
	 */
	public void setInteractive(bool value){
		interactive = value;
	}

	//! Public function getScore()
	/*! This function returns the current value of the score
	 */
	public float getScore(){
		return score;
	}

	//! Public function addScore()
	/*! This function adds points to the current temporal score when called
	 */
	public void addScore(float points){
		tempScore = tempScore + points;
	}

	//! Public function UpdateScore()
	/*! This function refreshes the global score using the combo multiplier and the temporal score
	 */
	public void UpdateScore(float multiplier){
		score += multiplier*tempScore;
		tempScore = 0;
	}

	//! Shoot() function
	/*! This function adds a force to "cube" GameObjects that were aimed and hit by the stone. Depending on the impact area
	 * the force applied will be greater (direct impact) or a normal hit (indirect impact). The "cube" gameobjects have 2 colliders
	 */
	void Shoot(Collider2D[] colliders){
		foreach (Collider2D collider in colliders){
			if(collider.transform.name == "Cube"){
				if(collider.GetType() == typeof(CircleCollider2D)){
					collider.GetComponent<Animator>().SetBool("isHit",true);
					collider.GetComponent<Rigidbody2D>().AddForceAtPosition((float)1.5*force*(collider.transform.position-this.transform.position).normalized,this.transform.position);
				}else if(collider.GetType() == typeof(BoxCollider2D)){
					if(!collider.GetComponent<Animator>().GetBool("isHit")){
						collider.GetComponent<Rigidbody2D>().AddForceAtPosition(force*(collider.transform.position-this.transform.position).normalized,this.transform.position);
					}
				}
			}
		}
	}

	//! OnTriggerEnter2D() function
	/*! This function controls the animator controller depending on the interaction of 
	 * the 2D colliders of the "cube" GameObjects
	 */
	void OnTriggerEnter2D(Collider2D other){
		if(other.transform.name == "Cube"){
			anim.SetBool("isAiming", true);
			other.GetComponent<Animator>().SetBool("isAimed",true);
			other.GetComponent<Animator>().SetBool("isShot",throwing);
		}
	}

	//! OnTriggerStay2D() function
	/*! This function controls the animator controller depending on the interaction of 
	 * the 2D colliders of the "cube" GameObjects
	 */
	void OnTriggerStay2D(Collider2D other){
		if(other.transform.name == "Cube"){
			anim.SetBool("isAiming", true);
			other.GetComponent<Animator>().SetBool("isAimed",true);
			other.GetComponent<Animator>().SetBool("isShot",throwing);
		}
	}

	//! OnTriggerExit2D() function
	/*! This function controls the animator controller depending on the interaction of 
	 * the 2D colliders of the "cube" GameObjects
	 */
	void OnTriggerExit2D(Collider2D other){
		if(other.transform.name == "Cube"){
			anim.SetBool("isAiming", false);
			other.GetComponent<Animator>().SetBool("isAimed",false);
			other.GetComponent<Animator>().SetBool("isShot",false);
		}
	}

	//! PlayAimSound() function
	/*! This function controls the audio clip playing
	 */
	void PlayAimSound(){
		this.GetComponent<AudioSource>().Stop();
		this.GetComponent<AudioSource>().Play();
		this.GetComponent<AudioSource>().PlayOneShot(audioAim);
	}

}
