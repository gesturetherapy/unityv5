﻿using UnityEngine;
using System.Collections;

//! HandBehaviour class
/*! This class represents the behaviour of the avatar and  GameObject "Aim" for position fllowing of the tracker
 *  It also describes the graphic behaviour of some events and it interprets the force of the pressure sensor of the "Gripper" input
 */
public class HandBehaviour : MonoBehaviour {

	float minForce; /*!< This variable is the minimum force stated by the Unity Editor  */
	float force; /*!< This variable is the current force obtained from the "Gripper" input */
	float maxForce; /*!< This variable is the maximum force stated by the Unity Editor */
	Animator anim; /*!< This variable represents the animator controller for throwing the stone */
	GameObject aim; /*!< This variable represents the GameObject "aim" */
	Vector3 initialPosition; /*!< This represents the initial position of the avatar */
	GameObject stone; /*!< This variable represents the GameObject "stone" for the animations */
	GameObject lineLeft; /*!< This Gameobjects is the left sling of the slingshot */
	GameObject lineRight; /*!< This Gameobjects is the right sling of the slingshot */
	GameObject GameHandler; /*!< This GameObjects is the communicator between this script and the "CubesHandler" script  */
	Vector3 stoneInitialPosition; /*!< This variable is the initial position of the stone */
	Vector3 lineLeftScale; /*!< This variable is the current scale of the left sling of the slingshot */
	Vector3 lineRightScale; /*!< This variable is the current scale of the right sling of the slingshot */
	Vector3 lineLeftRotation; /*!< This variable is the current rotation of the left sling of the slingshot */
	Vector3 lineRightRotation; /*!< This variable is the current rotation of the right sling of the slingshot */
	bool animate; /*!< This flag starts the throwing animation of the "stone", the "slingshot" and "hand" GameObjects */
	float step; /*!< This variable represents the speed of the animation of the stone */
	bool stretchable; /*!< This flag is used in order to play the different audio sources */
	public AudioClip throwingClip; /*!< This variable represents the stretch audio clip that will be played */
	public AudioClip thrownClip; /*!< This variable represents the throw audio clip that will be played */
	bool interactive; /*!< This flag is used afteer each throw and after the score is updated in each turn */

	//! Start() function
	/*! This function is called on every Scene change in Unity 
	 * it initializes the value of the variables described above
	 * excepting the public variables initialized in the Unity editor
	 */
	void Start () {
		anim = this.GetComponent<Animator>();
		aim = GameObject.Find("Aim");
		initialPosition = this.transform.localPosition;
		minForce = aim.GetComponent<AimControllerScript>().minForce;
		force = aim.GetComponent<AimControllerScript>().force;
		maxForce = aim.GetComponent<AimControllerScript>().maxForce;
		stone = GameObject.Find("Stone");
		lineLeft = GameObject.Find ("LineLeft");
		lineRight = GameObject.Find ("LineRight");
		GameHandler = GameObject.Find("GameHandler");
		stoneInitialPosition = stone.transform.localPosition;
		lineLeftScale = lineLeft.transform.localScale;
		lineRightScale = lineRight.transform.localScale;
		lineLeftRotation = lineLeft.transform.rotation.eulerAngles;
		lineRightRotation = lineRight.transform.rotation.eulerAngles;
		animate = false;
		stretchable = true;
		step = 0.0f;
		interactive = true;
	}
	
	//! Update() function
	/*! This function is called every frame, it refreshes the
	 * animation variables, the forces, animation of the stone and slingshot, 
	 * audioclip playing and sets communication with the script "CubesHandler"
	 */
	void Update () {
		step = 50 * Time.deltaTime;
		minForce = aim.GetComponent<AimControllerScript>().minForce;
		force = aim.GetComponent<AimControllerScript>().force;
		maxForce = aim.GetComponent<AimControllerScript>().maxForce;
		this.transform.localScale = new Vector3(force/minForce,force/minForce,0);
		if(interactive){
			if(aim.GetComponent<PDController>().Hold()){
				anim.SetBool("isThrowing",true);
				this.transform.localPosition = new Vector3(initialPosition.x+this.transform.localScale.x/8,initialPosition.y-this.transform.localScale.y,0);
				lineLeft.transform.localScale = new Vector3(lineLeft.transform.localScale.x,lineLeftScale.y+(force/minForce)/4,0);
				lineRight.transform.localScale = new Vector3(lineRight.transform.localScale.x,lineRightScale.y+(force/minForce)/4,0);
				lineLeft.transform.eulerAngles = new Vector3(0,0,lineLeftRotation.z-10f*(force/minForce));
				lineRight.transform.eulerAngles = new Vector3(0,0,lineRightRotation.z+5f*(force/minForce));
			}else{
				if(anim.GetBool("isThrowing")){
					animate = true;
					interactive = false;
					lineLeft.transform.localScale = lineLeftScale;
					lineRight.transform.localScale = lineRightScale;
					lineLeft.transform.eulerAngles = lineLeftRotation;
					lineRight.transform.eulerAngles = lineRightRotation;
				}
				anim.SetBool("isThrowing",false);
			}
		}
		if(animate){
			this.transform.localPosition = Vector3.MoveTowards(this.transform.localPosition, initialPosition,step);
			if(force <= maxForce/2){
				PlaySoundThrown();
				stone.transform.position = Vector3.MoveTowards(stone.transform.position,aim.transform.position,step);
				if(stone.transform.position == aim.transform.position){
					stone.transform.localPosition = stoneInitialPosition;
					GameHandler.GetComponent<CubesHandler>().StartEndOfTurn();
					animate = false;
				}
			}
		}
	}

	//! Public function ResetPositionScale()
	/*! This function is called after each turn is finished and resets the scale and position 
	 * of the avatar and "stone" Gameobjects 
	 */
	public void ResetPositionScale(){
		this.transform.localPosition = initialPosition;
		this.transform.localScale = new Vector3(1,1,0);
		stone.transform.localPosition = stoneInitialPosition;
	}

	//! SetInteractive() function
	/*! This function is called when the "stone" Gameobject is thrown and it blocks the avatar, 
	 * so it doesn't thrown another stone while the "cube" GamObjects are moving or falling 
	 */
	public void setInteractive(bool value){
		interactive = value;
	}

	//! PlaySoundThrowing() function
	/*! This function is called when the stone is being thrown while pressing the pressure sensor
	 */
	void PlaySoundThrowing(){
		if(!this.GetComponent<AudioSource>().isPlaying && stretchable){
			stretchable = false;
			this.GetComponent<AudioSource>().PlayOneShot(throwingClip);
		}
	}

	//! PlaySoundThrown() function
	/*! This function is called when the stone was thrown while releasing the pressure sensor
	 */
	void PlaySoundThrown(){
		this.GetComponent<AudioSource>().Stop();
		this.GetComponent<AudioSource>().PlayOneShot(thrownClip);
		stretchable = true;
	}

}
