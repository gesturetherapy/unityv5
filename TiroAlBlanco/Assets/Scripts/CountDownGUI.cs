﻿using UnityEngine;
using System.Collections;

//! CountDownGUI class
/*! This class contains the first graphic interface for the user  once the splash screen of Unity disappears
 * The countdown starts at 3 seconds and after that time the first scene or level of the game is loaded
 */
public class CountDownGUI : MonoBehaviour {
	
	private static float countdown; /*!< This variale represents the timer shown in the GUI starting with 3 seconds  */
	public GUIStyle style; /*!< This represents the text style of the countdown GUI */
	public Texture2D black; /*!< This texture is the background of scene, this can be edited directly in the Unity editor */
	private static byte alpha; /*!< The trasparency value (0-255) for the countdown interface */
	private static string previousValue; /*!< Previous value of the counter in integers */


	//! Start() function
	/*! This function is called on every Scene change in Unity 
	 * it initializes the value of the variables described above
	 * excepting the public variables initialized in the Unity editor
	 */
	void Start () {
		countdown = 3.5f;
		if (Time.timeScale == 0)
			Time.timeScale = 1;
		alpha = 255;
		previousValue = countdown.ToString("0");
	}
	
	//! FixedUpdate() function
	/*! This function is called every frame it refreshes the
	 * the control of turns start here as well as the variable
	 * refreshing  once per time step in Unity Editor(0.02 seconds by default)
	 */
	void FixedUpdate () {
		if(alpha > 5)
			alpha -= 5;
		if(countdown.ToString("0") != previousValue){
			alpha = 255;
			previousValue = countdown.ToString("0");
		}
		if(countdown > -0.49){
			countdown -= Time.fixedDeltaTime;
		} else {
			Application.LoadLevel("Difficulty 1");
		}
	}

	//!OnGUI() function
	/*! This script contains the most of the GUI and feedback of the game, this includes 2d textures,
	 * scores, time remaining, audio to play, loading levels, countdowns or quitting application. Most of this GUI layer
	 * is planned for resolution of 1024*768
	 */
	void OnGUI(){
		GUI.color = new Color32(255, 255, 255, 255);
		GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),black);
		GUI.color = new Color32(255, 255, 255, alpha);
		GUI.TextField(new Rect(0,0,Screen.width,Screen.height),countdown.ToString("0"),style);
	}

}
