﻿using UnityEngine;
using System.Collections;

//! CubeBehaviour class
/*! This class represents the behaviour of each Gameobject "cube" in the scene
 * the Unity 2DPhysics, playing audio clips and interaction with animation controller 
 * are some of the characteristics of this script
 */
public class CubeBehaviour : MonoBehaviour {

	Rigidbody2D physics2D; /*!< This variable repreents the RigidBody2D component of each prefab "cube" */
	Animator anim; /*!< This variable repreents the Animator component of each prefab "cube" */
	Vector3 onScreenCheck; /*!< This variable is used in order to know if the cube is still on the screen */
	bool isAbleToScream; /*!< This variable is used as a flag if there's no other audio clip currently playing */
	bool isAbleToMock; /*!< This variable is used as a flag if there's no other audio clip currently playing */
	public AudioClip screamClip; /*!< This variable is the audio clip used for screaming animation */
	public AudioClip mockClip; /*!< This variable is the audio clip used for mocking animation */
	public Transform score50Prefab; /*!< This variable is the prefab for 50 points score after the cube is destroyed */
	public Transform score100Prefab; /*!< This variable is the prefab for 100 points score after the cube is destroyed */

	//! Start() function
	/*! This function is called on every Scene change in Unity 
	 * it initializes the value of the variables described above
	 * excepting the public variables initialized in the Unity editor
	 */
	void Start () {
		physics2D = this.gameObject.GetComponent<Rigidbody2D>();
		anim = this.gameObject.GetComponent<Animator>();
		isAbleToScream = true;
		isAbleToMock = true;
	}
	
	//! Update() function
	/*! This function is called every frame, it refreshes the
	 * animation variables, On-Screen check and the destruction of the GameObject "cube"
	 */
	void Update () {
		anim.SetFloat("Speed",physics2D.velocity.magnitude);
		anim.SetFloat("angularSpeed",physics2D.angularVelocity);
		onScreenCheck = Camera.main.WorldToScreenPoint(this.gameObject.transform.position);
		if(!(onScreenCheck.x >= 0 && onScreenCheck.x <= Screen.width) || !(onScreenCheck.y >= 0 && onScreenCheck.y <= Screen.height)){
			IndirectDestroy();
		}
	}

	//! IndirectDestroy() function
	/*! This function is called when the cube is out of the screen because the interaction with other cubes
	 */
	void IndirectDestroy(){
		if(!anim.GetBool("isHit")){
			GameObject.Find("Aim").GetComponent<AimControllerScript>().addScore(50);
			Instantiate(score50Prefab,this.transform.position,Quaternion.identity);
			Destroy(this.gameObject);
		}
	}

	//! DestroyCube() function
	/*! This function is called when the cube is out of the screen because it was hit directly at the center
	 */
	void DestroyCube(){
		GameObject.Find("Aim").GetComponent<AimControllerScript>().addScore(100);
		Instantiate(score100Prefab,this.transform.position,Quaternion.identity);
		Destroy(this.gameObject);
	}

	//! DisableColliders() function
	/*! This function disables the colliders of the GameObjec "cube" after a direct hit
	 * so it doesn't interact with the other GameObjects hit indirectly
	*/
	void DisableColliders(){
		this.GetComponent<SpriteRenderer>().sortingOrder = -1;
		Collider2D[] colliders = this.GetComponents<Collider2D>();
		foreach(Collider2D collider in colliders){
			collider.enabled = false;
		}
	}

	//! PlayScreamAudio() function
	/*! When this fucntion is called it plays an audio clip if there's not playing another one already
	 */
	void PlayScreamAudio(){
		if(isAbleToScream)
			this.GetComponent<AudioSource>().PlayOneShot(screamClip);
		isAbleToScream = false;
	}

	//! PlayMockAudio() function
	/*! When this fucntion is called it plays an audio clip if there's not playing another one already
	 */
	void PlayMockAudio(){
		if(isAbleToMock)
			this.GetComponent<AudioSource>().PlayOneShot(mockClip);
		isAbleToMock = false;
	}

	//! StopAudio() function
	/*! This functions stops any audio source playing when it's called
	 */
	void StopAudio(){
		this.GetComponent<AudioSource>().Stop();
		isAbleToScream = true;
		isAbleToMock = true;
	}

}
