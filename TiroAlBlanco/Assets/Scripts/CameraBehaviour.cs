﻿using UnityEngine;
using System.Collections;

//! cameraBehaviour class
/*! This class describes the behaviour of most of the GUI of the game,
 * the time left in seconds of the game, it also handles when to change
 * the difficulty of the game and the object destroying after the game
 */
public class CameraBehaviour : MonoBehaviour {

	float score; /*!< Current value of the points in the current scene */
	GameObject aim; /*!< GameObject of the avatar */
	GameObject gameHandler; /*!< GameObject of the handler of the "cubes" described in CubesHandler class */
	GameObject[] cubes; /*!< GameObject array of the current "cubes" */
	GameObject[] boards; /*!< GameObject of the "boards" */
	public string nextLevel; /*!< String pointer for the next difficulty  */
	public GUIStyle customBoxTime; /*!< GUIStyle for the remaining time in OnGUI() fucntion */
	public GUIStyle customBoxScore; /*!< GUIStyle for the score and remaining "cubes" in OnGUI() function */
	public Texture2D texture; /*!< 2D texture for the charging bar */
	public Texture2D gripperTexture; /*!< 2D texture for the gripper icon */
	public Texture2D chargeTexture; /*!< 2D texture for the charging texture */
	static float timeLeftSeconds = 180.0f; /*!< Time remaining in seconds for the game to finish */
	static float startingTimeLeftSeconds = timeLeftSeconds; /*!< Starting time of the whole game */
	public float minForce; /*!< Minimum force that the slingshot can generate set in the Unity editor */
	public float force; /*!< Current force that the slingshot is generating set in the Unity editor */
	public float maxForce; /*!< Maximum force that the slingshot can generate set in the Unity editor */
	int hit; /*!< Current number of GameObjects "cubes" destroyed durring this scene */
	bool stop; /*!< This variable describes when the regressive countdown pauses*/
	bool gameOver; /*!< This variable is set to true when the variable "TimeLeftSeconds" reaches 0.0f */
	bool gameSucceeded; /*!< This variable is set to true when the number of remaining "cubes" in this scene is 0 */
	bool wasPlayed; /*!< This variable is set to true when the script already played the AudioClip of the properties of this gameObject  */
	public GUIStyle customTitle; /*!< GUIStyle of the title at the end of a scene */
	public GUIStyle customButton; /*!< GUIStyle of the buttons at the end of a scene */
	bool startCount; /*!< This variable is set to true when the game succeds, this means that there are no "cubes" left in the current scene */
	float GUIcount; /*!< Time remaining after the game succeeds to jump to the next scene or difficulty set in the variable "nextLevel" */

	//! Start() function
	/*! This function is called on every Scene change in Unity 
	 * it initializes the value of the variables described above
	 * excepting the public variables initialized in the Unity editor
	*/
	void Start () {
		score = 0.0f;
		aim = GameObject.Find("Aim");
		gameHandler = GameObject.Find("GameHandler");
		minForce = aim.GetComponent<AimControllerScript>().minForce;
		force = aim.GetComponent<AimControllerScript>().force;
		maxForce = aim.GetComponent<AimControllerScript>().maxForce;
		hit = 0;
		stop = false;
		gameOver = false;
		gameSucceeded = false;
		wasPlayed = false;
		startCount = false;
		GUIcount = 3.0f;
	}
	
	//! Update() function
	/*! This function is called every frame it refreshes the
	 * time countdown, forces and the destruction of GameObjects
	 */
	void Update () {
		//! GUIcount countdown
		/*! If variable startCount is set to true the GUIcount will start and 
		 * when it reaches 0 the game will load the next scene
		 */
		if(startCount){
			GUIcount -= Time.deltaTime;
		}
		//! CameraBehaviour handles
		/*! The class upgrades the current force on each frame, the remaining time in seconds, 
		 * the score, the number of "cubes" destroyed in this scene and also if the time remaining
		 * reaches 0 this script destroys the "cubes" and "boards"
		 */
		if(!gameSucceeded){
			if(timeLeftSeconds > 0.0f){
				if(!stop)
					timeLeftSeconds -= Time.deltaTime;
				minForce = aim.GetComponent<AimControllerScript>().minForce;
				force = aim.GetComponent<AimControllerScript>().force;
				maxForce = aim.GetComponent<AimControllerScript>().maxForce;
				score = aim.GetComponent<AimControllerScript>().getScore();
				hit = gameHandler.GetComponent<CubesHandler>().getDestroyed();
			}else{
				GameObject.Destroy(aim);
				GameObject.Destroy(gameHandler);
				cubes = GameObject.FindGameObjectsWithTag("Cubes");
				foreach(GameObject cube in cubes){
					GameObject.Destroy (cube);
				}
				boards = GameObject.FindGameObjectsWithTag("Boards");
				foreach(GameObject board in boards){
					GameObject.Destroy (board);
				}
				gameOver = true;
			}
			}
	}

	//! Public function GameSucceed()
	/*! This function is called by the GubesHandler class after the player has destroyed  
	 * all the "cubes" destroys the GameObjects "boards", the time left in secods multiplied by
	 * 5 is added to the score at the end.
	 */
	public void GameSucceed(){
		GameObject.Destroy(aim);
		GameObject.Destroy(gameHandler);
		boards = GameObject.FindGameObjectsWithTag("Boards");
		foreach(GameObject board in boards){
			GameObject.Destroy (board);
		}
		score += 5*Mathf.RoundToInt(timeLeftSeconds);
		gameSucceeded = true;
	}

	//! Public function GetStartingTimeLeft() 
	/*! Returns the variable "startingTimeLeftSeconds"
	 */
	public float GetStartingTimeLeft(){
		return startingTimeLeftSeconds;
	}

	//! Public function GetSTimeLeft() 
	/*! Returns the variable "timeLeftSeconds"
	 */
	public float GetTimeLeft(){
		return timeLeftSeconds;
	}

	//! Public function Stop()
	/*! This function is called in order to resume/pause the countdown of the variable "timeLeftSeconds", 
	 * for example if a turn has ended 
	 */
	public void Stop(bool value){
		stop = value;
	}

	//!OnGUI() function
	/*! This script contains the most of the GUI and feedback of the game, this includes 2d textures,
	 * scores, time remaining, audio to play, loading levels or quitting application. Most of this GUI layer
	 * is planned for resolution of 1024*768
	 */
	void OnGUI(){
		if(!gameOver && !gameSucceeded){
			GUI.DrawTexture(new Rect(70,90,texture.width-200,texture.height-20),texture);
			GUI.DrawTexture(new Rect(70,90,((force-minForce)/(maxForce-minForce))*(texture.width-200),texture.height-20),chargeTexture);
			GUI.DrawTexture(new Rect(20,80,gripperTexture.width/2,gripperTexture.height/4),gripperTexture);
			GUI.TextField(new Rect(Screen.width-140,100,0,0),("Score: "+score.ToString("00000")),customBoxScore);
			GUI.TextField(new Rect(Screen.width-140,140,0,0),("Hits x "+hit.ToString("00")),customBoxScore);
		} else if (gameOver){
			GUI.BeginGroup(new Rect(Screen.width/2-Screen.width/4,Screen.height/2-Screen.height/4,Screen.width/2,Screen.height/2));
			GUI.Box (new Rect(0,0,Screen.width/2,Screen.height/8),"Game Over!\n\r"+Application.loadedLevelName+" Score: "+score.ToString("00000"),customTitle);
			startCount = true;
			GUI.Box(new Rect(10,(Screen.height/2-Screen.height/4)/4+100,Screen.width/2-20,Screen.height/8-20),"Exits in: "+GUIcount.ToString("0"),customButton);
			if(GUIcount <= 0.0f){
				Application.Quit();
			}
			GUI.EndGroup ();
		} else if(gameSucceeded){
			if(!wasPlayed)
			{
				if(!this.GetComponent<AudioSource>().isPlaying)
					this.GetComponent<AudioSource>().Play();
				wasPlayed = true;
			}
			GUI.BeginGroup(new Rect(Screen.width/2-Screen.width/4,Screen.height/2-Screen.height/4,Screen.width/2,Screen.height/2));
			GUI.Box (new Rect(0,0,Screen.width/2,Screen.height/8),"Congratulations!\n\r"+Application.loadedLevelName+" Score: "+score.ToString("00000"),customTitle);
			startCount = true;
			if(nextLevel == "Quit"){
				GUI.Box(new Rect(10,(Screen.height/2-Screen.height/4)/4+100,Screen.width/2-20,Screen.height/8-20),"Exits in: "+GUIcount.ToString("0"),customButton);
			} else{
				GUI.Box(new Rect(10,(Screen.height/2-Screen.height/4)/4+100,Screen.width/2-20,Screen.height/8-20),"Next Level in: "+GUIcount.ToString("0"),customButton);
			}
			if(GUIcount <= 0.0f){
				if(nextLevel == "Quit"){
					Application.Quit();
				} else{
					Application.LoadLevel(nextLevel);
				}
			}
			GUI.EndGroup ();
		}
	}

}
