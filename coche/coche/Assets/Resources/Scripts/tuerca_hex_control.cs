﻿using UnityEngine;
using System.Collections;

public class tuerca_hex_control : MonoBehaviour {

	public GameObject thePrefab;
	GameObject instance;
	Vector3 init_pos;
	void Start(){
		instance = new GameObject();
		init_pos = new Vector3(-0.00129396f,-0.004246143f,0f);
	}
	bool seen = false;
	void Update(){
		if(GetComponent<Renderer>().isVisible)
			seen = true;
		if(!GetComponent<Renderer>().isVisible && seen){
			seen = false;
			Destroy(gameObject);
			instance = Instantiate(thePrefab, init_pos, transform.rotation) as GameObject;
			instance.GetComponent<Rigidbody2D>().isKinematic=true;
			instance.transform.position = init_pos;
			//instance.transform.localScale = new Vector3(1.538462f,1.538462f,1);
		}		
	}
}
