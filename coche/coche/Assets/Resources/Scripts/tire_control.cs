﻿using UnityEngine;
using System.Collections;

public class tire_control : MonoBehaviour {

	// Use this for initialization
	public AudioClip tire_sound;
	public Sprite tire_new;
	Vector3 tire_pos;
	bool seen = false;
	void Start(){
		tire_pos = gameObject.transform.position;
	}
	void Update(){
		if(GetComponent<Renderer>().isVisible)
			seen = true;
		if(!GetComponent<Renderer>().isVisible && seen){
			seen = false;
			//Destroy(gameObject);
			gameObject.GetComponent<Rigidbody2D>().isKinematic=true;
			//tire_sprite = Resources.Load<Sprite>(path_tire[remove0_put1]);
			gameObject.GetComponent<SpriteRenderer>().sprite = tire_new;
			gameObject.transform.position = tire_pos;
			gameObject.transform.rotation = Quaternion.Euler(new Vector3(0,0,0));
			GameObject.Find("matraca").GetComponent<control>().end_car = false;
		}		
	}
	
	void OnCollisionEnter2D (Collision2D other){
		if(other.gameObject.name == "floor")
			GetComponent<AudioSource>().PlayOneShot(tire_sound);
	}
}