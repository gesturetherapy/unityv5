﻿using UnityEngine;
using System.Collections;

public class tuerca_control : MonoBehaviour {

	// Use this for initialization
	public AudioClip bounce_sound;
	bool seen = false;
	void Update(){
		if(GetComponent<Renderer>().isVisible)
			seen = true;
		if(!GetComponent<Renderer>().isVisible && seen){
			seen = false;
			Destroy(gameObject);
		}		
	}

	void OnCollisionEnter2D (Collision2D other){
		GetComponent<AudioSource>().PlayOneShot(bounce_sound);
	}
	
}
