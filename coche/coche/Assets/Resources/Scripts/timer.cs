﻿using UnityEngine;
using System.Collections;

public class timer : MonoBehaviour {

	// Use this for initialization
	public float temporizador;
	static float myTimer = -1;
	int min, sec;
	void Start(){
		if (myTimer==-1)
			myTimer = temporizador;
	}
	// Update is called once per frame
	void Update () {
		min = (int)myTimer / 60;
		sec = (int)myTimer % 60;
		if(sec > 9)
			GetComponent<GUIText>().text = "Time: "+min+":"+sec;
		else
			GetComponent<GUIText>().text = "Time: "+min+":0"+sec;
		if (myTimer>=0){
			myTimer -= Time.deltaTime;
		}
		if(myTimer<0 && myTimer>-6){
			Destroy(GameObject.Find("matraca"));
			Destroy(GameObject.Find("matraca_bg"));
			Destroy(GameObject.Find("arco_der"));
			Destroy(GameObject.Find("tuerca_hex"));
			myTimer -= Time.deltaTime;
			GetComponent<GUIText>().transform.position = new Vector3(0.5f,0.5f,0f);
			GetComponent<GUIText>().text = "Juego terminado";
			GetComponent<GUIText>().fontSize = 70;
			GetComponent<GUIText>().fontStyle = FontStyle.Bold;
			GetComponent<GUIText>().color = new Color(0f,0f,0f);
			//Debug.Log("Game over");
		}
		if(myTimer<-5){
			//guiText.text = "Salir";
			Application.Quit();
		}
	}
}
