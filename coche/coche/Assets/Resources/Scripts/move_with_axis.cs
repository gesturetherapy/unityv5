﻿using UnityEngine;
using System.Collections;

public class move_with_axis : MonoBehaviour {

	// Use this for initialization
public float speed;
public bool draw=false;
public	float x,y,xpix,ypix;
float hsize,vsize;
int stage;
float traslationX, traslationY;
bool init = true;
float [] xbound_min = new float[2];
float [] xbound_max = new float[2];
float [] ybound_min = new float[2];
float [] ybound_max = new float[2];
	// Update is called once per frame

	void FixedUpdate () {

		vsize = GameObject.Find("Main Camera").GetComponent<Camera>().orthographicSize*2;
		hsize = vsize * Screen.width/Screen.height;
		stage = GameObject.Find("matraca").GetComponent<control>().remove0_put1;
		
		xbound_min[0] = 0 - 0.2f;
		xbound_max[0] = hsize/2f;
		ybound_min[0] = 0 - 0.2f;
		ybound_max[0] = vsize/2f;
		
		xbound_min[1] = -hsize/2f;
		xbound_max[1] = 0 + 0.2f;
		ybound_min[1] = 0 - 0.2f;
		ybound_max[1] = vsize/2f;
		if (init){
			x = xbound_min[stage]+0.5f;//xbound_min[stage];
			y = ybound_max[stage];
			init = false;
		}
		
		if (y <= ybound_max[stage] && y >= ybound_min[stage]){
			traslationY = Input.GetAxis("Vertical")*speed;
			traslationY *= Time.deltaTime;
		}
		y += traslationY; 
		ypix = Screen.height / vsize * y;
		if (y < ybound_min[stage])
			y = ybound_min[stage];
		if (y >ybound_max[stage])
			y = ybound_max[stage];

		traslationX = Input.GetAxis("Horizontal")*speed;
		traslationX *= Time.deltaTime;
		
		x += traslationX; 
		xpix = Screen.width / hsize * x;
		if (x < xbound_min[stage])
			x = xbound_min[stage];
		if (x >xbound_max[stage])
			x = xbound_max[stage];
		
	}
	void Update(){
		if(draw){
			transform.position = new Vector3(x,y,0);
			transform.Translate(new Vector3(0,traslationY,0));
			transform.Translate(new Vector3(traslationX,0,0));
		}
		else {
			gameObject.GetComponent<Renderer>().enabled = false;
		}
	}
}
